//Ryan Hartsock
#include<string>
using namespace std;

class Person
{
private:
	
	string birth;
	string status;
	string spouseName;
	string spouseBirth;
	string children[10];
	string death;
	

public:
	string name;
	int childCount;
	Person(){name = "";};
	Person(string s){ name = s;};
	
	void setName(string n);
	void setBirth(string b);
	void setStatus(string s);
	void setSpouseName(string sn);
	void setSpouseBirth(string sb);
	void addChild(int i, string c);
	void addDeath(string d);

	bool checkStatus();
	void updateChildCount();
	
	void printName();
	void printBirth();
	void printStatus();
	void printSpouseName();
	void printSpouseBirth();
	void printChild(int cnt);
	void printDeath();
};

